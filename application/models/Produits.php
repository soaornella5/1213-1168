<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    // require('Mere.php');
    /*
    idP int not null auto_increment,
    idCat int,
    nom varchar(50),
    prixUnitaire decimal,
    img varchar(50),
    */
    class Produits extends CI_Model{
        private $idP;
        private $idCat;
        private $nom;
        private $prixUnitaire;
        private $img;

        /*public function insert(){
            $cur='curdate()';
            $req=('insert into achat (idCaisse,idProduit,quantite,dateAchat) VALUES ('.$this->getIdCaisse().','.$this->getIdProduit().','.$this->getQuantite().','.$cur.')');
            $this->db->query($req);
        }*/

        public function getPrixUnitaire() { 
            return $this->prixUnitaire; 
        } 

        public function setPrixUnitaire($id) {  
            $this->prixUnitaire = $id; 
        }
        
        public function getImg() { 
            return $this->img;
        } 

        public function setImg($id) {  
            $this->img = $id; 
        } 

        public function getNom() { 
            return $this->nom; 
        } 

        public function setNom($id) {  
            $this->nom = $id; 
        } 
        
        public function getIdP() { 
            return $this->idP; 
        } 

        public function setIdP($id) {  
            $this->idP = $id; 
        } 

        public function getIdCat() { 
            return $this->idCat; 
        } 

        public function setIdCat($id) {  
            $this->idCat = $id; 
        } 


      

       
}
?>