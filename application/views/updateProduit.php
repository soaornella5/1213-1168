<div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Modifier Produit</h4>
                  <form class="form-sample" action="<?php echo site_url("globalController/updateProduit") ?>" method="$post">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nom du Produit</label>
                          <div class="col-sm-9">
                          <?php $uu=$idp  ?>
                          <input type="hidden" value="<?php echo $uu; ?>" name="before">
                            <input type="text" class="form-control" name="nom" value="<?php echo $uu ?>"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">nom du fichier(Image)</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="image" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Categorie</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="categorie">
                              <?php 
                              $r=$cat;
                              for($i=1;$i<=count($r);$i++) {?>
                              <option value="<?php echo $r[$i]['idCat'] ?>"><?php echo $r[$i]['nom'] ?></option>
                              <?php } ?>                    
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Prix Unitaire</label>
                          <div class="col-sm-9">
                            <input class="form-control" name="prix" type="text" value=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                   <center> <button type="submit" class="btn btn-primary mr-2">Modifier</button> </center>
                      
                  </form>
                </div>
              </div>
            </div>