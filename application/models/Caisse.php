<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    // require('Mere.php');
    /*
     idC int,
    numero int,
    nom varchar(50),
    */
    class Caisse extends CI_Model{
        private $idC;
        private $numero;
        private $nom;

        /*public function insert(){
            $cur='curdate()';
            $req=('insert into achat (idCaisse,idProduit,quantite,dateAchat) VALUES ('.$this->getIdCaisse().','.$this->getIdProduit().','.$this->getQuantite().','.$cur.')');
            $this->db->query($req);
        }*/

        public function getIdC() { 
            return $this->idC; 
        } 

        public function setIdC($id) {  
            $this->idC = $id; 
        }

        public function getNumero() { 
            return $this->numero; 
        } 

        public function setNumero($id) {  
            $this->numero = $id;
        }
        
        public function getNom() { 
            return $this->nom; 
        } 

        public function setNom($id) {  
            $this->nom = $id;
        }
       
}
?>