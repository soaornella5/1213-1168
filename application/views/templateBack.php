e<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Ma.caisse</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo vendor("mdi/css/materialdesignicons.min.css") ?>">
  <link rel="stylesheet" href="<?php echo vendor("base/vendor.bundle.base.css"); ?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo vendor("datatables.net-bs4/dataTables.bootstrap4.css"); ?>">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo css("style.css") ?>">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
<?php echo css("style.css"); ?>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-brand-wrapper d-flex justify-content-center">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
          <a class="navbar-brand brand-logo" href="index.html"><img src="<?php echo sary("logo.png") ?>" alt="logo"/></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?php echo sary("logo.png") ?>" alt="logo"/></a>
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-sort-variant"></span>
          </button>
        </div>  
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav mr-lg-4 w-100">
          <li class="nav-item nav-search d-none d-lg-block w-100">
            <form action="<?php echo site_url("globalController/searchSimple") ?>" method="post">
            <div class="input-group">
              
              
              <div class="input-group-prepend">
                
                <span class="input-group-text" id="search">
                  <!-- <button type="submit" class="btn btn-light" style="width:15px;height:20px;background-color: rgb(236,236,236;border-color: rgb(236,236,236);">--><i class="mdi mdi-magnify"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Rechercher" name="mot" aria-label="search" aria-describedby="search">
            </form>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          
         
          <li class="nav-item nav-profile dropdown">
            <p  data-toggle="dropdown" id="profileDropdown">
            
              <span class="nav-profile-name" style="margin-left: 10px;">Admin</span>
            </p>
            
          </li>
        </ul>
       
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('accueil/template') ?>">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Accueil</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-chart-pie menu-icon"></i>
              <span class="menu-title">Statistiques</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('globalController/globalStat') ?>">Global</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('globalController/globalProduit') ?>">Produit</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('globalController/lienParam') ?>">
              <i class="mdi mdi-view-headline menu-icon"></i>
              <span class="menu-title">Parametrage</span>
            </a>
          </li>
          <li class="nav-item">
            <p class="nav-link" href="#">
              <i class="mdi mdi-magnify menu-icon"></i>
              <span class="menu-title">Recherche</span>
            </p>
            <br>
            <form action="<?php echo site_url("globalController/filtre")?>" method="post">
            <center><p> <strong>Categorie:</strong> <select name="cat" style="width:80px;margin-bottom: 10px;"> 
            <?php for($i=1;$i<=count($nomCat);$i++) { ?>
            <option value="<?php echo $nomCat[$i]['idCat'] ?>"><?php echo $nomCat[$i]['nom'] ?></option> 
            <?php } ?>
          </select></p></center> 
            <center><p> <strong>Prix min:</strong> <input type="text" name="minimum" style="width:80px;height:20px;margin-left: 10px;margin-bottom: 10px;"></p></center>
            <center><p> <strong>Prix max:</strong> <input type="text" name="max" style="width:80px;height:20px;margin-left: 10px;margin-bottom: 10px;"></p></center>
            <center> <button type="submit" class="btn btn-primary mr-2" style="height:35px;background-color: white;color:black;"> Filtrer</button></center>
          </form>
          <br><br>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('globalController/listeProduit') ?>">
              <i class="mdi mdi-grid-large menu-icon"></i>
              <span class="menu-title">Produits</span>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          
        <?php 
        
        $this->load->view($page); 
       
        ?>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright ©Nick et Soa</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> ETU1168 && ETU1213</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo vendor("base/vendor.bundle.base.js")?>"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="<?php echo vendor("chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo vendor("datatables.net/jquery.dataTables.js") ?>"></script>
  <script src="<?php echo vendor("datatables.net-bs4/dataTables.bootstrap4.js")?>"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo js("off-canvas") ?>"></script>
  <script src="<?php echo js("hoverable-collapse") ?>"></script>
  <script src="<?php echo js("template.js")?>"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo js("dashboard") ?>"></script>
  <script src="<?php echo js("data-table") ?>"></script>
  <script src="<?php echo js("jquery.dataTables") ?>"></script>
  <script src="<?php echo js("dataTables.bootstrap4") ?>"></script>
  <!-- End custom js for this page-->
  <script src="<?php echo js("jquery.cookie")?>" type="text/javascript"></script>
</body>

</html>

