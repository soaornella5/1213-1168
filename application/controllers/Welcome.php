<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url_helper');
		$this->load->view('welcome_message');
		
	}
	public function index2()
	{
		$this->load->view('Bien');
		
	}	

	public function index3($text1,$text2)
	{
		echo $_GET['id'];
		echo $text1;
		echo $text2;	
		//$this->load->view('welcome_message');
		
	}	

	public function index4()
	{
		$data['page']="Bienvenue";
		$this->load->helper('url_helper');
		$this->load->view('template',$data);
	}	

}
