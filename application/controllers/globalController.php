<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class globalController extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}
    public function searchSimple()
    {
        $data=array();
        $this->load->model('Fonction');
        $c=new Fonction();
        $mot=$this->input->post("mot");
        $data['resultat']=$c->rechercheProduitSimple($mot);
        $data['page']='ResultatSimple';
        // echo $data['resultat'];
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }
    public function filtre()
    {
        $data=array();
        $this->load->model('Fonction');
        $c=new Fonction();
        $cat=$this->input->post("cat");
        $min=$this->input->post("minimum");
        $max=$this->input->post("max");
        $data['res']=$c->filtre($cat,$min,$max);
        $data['page']='ResultatFiltre';
        $data['nomCat']=$c->getCategorie();
        // echo $data['res'];
        $this->load->view('templateBack',$data);
    }
    public function globalStat()
    {
        $data=array();
        $this->load->model('Fonction');
		$c=new Fonction();
        $data['res']=$c->top3Global();
        $data['page']='StatGlobal';
        $this->load->helper('css_helper');
        $data['nomCat']=$c->getCategorie();
		$this->load->view('templateBack',$data);
    }
    public function globalProduit()
    {
        $data=array();
        $this->load->model('Fonction');
		$c=new Fonction();
        $data['nomCat']=$c->getCategorie();
        $data['res']=$c->statProduit();
        $data['page']='Statproduit';
        $this->load->helper('css_helper');
		$this->load->view('templateBack',$data);
    }
    public function listeProduit(){
        $data=array();
        $this->load->model('Fonction');
		$c=new Fonction();
        $data['dtP']=$c->getDetailsProduits();
        $data['nomCat']=$c->getCategorie();
        $this->load->helper('css_helper');
        $data['page']='ListeProduits.php';

		$this->load->view('templateBack',$data);
    }

    public function ajoutProduit(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $rep=$c->getCategorie();
        $data=array();
        $data['cat']=$rep;
        $data['page']='insertProduit.php';
        $data['nomCat']=$c->getCategorie();
		$this->load->view('templateBack',$data);
    }

    public function modifier(){
        $this->load->model('Fonction');
        $c=new Fonction();
        $rep=$c->getCategorie();
        $data=array();
        $data['cat']=$rep;
        $data['idp']=$this->input->post("idp");
        $data['iddp']=$this->input->post("iddp");
        $data['page']='updateProduit.php';
        $data['nomCat']=$c->getCategorie();
		$this->load->view('templateBack',$data);
    }


    public function ajouterProduit(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $idCat=$this->input->get("categorie");
        $nom=$this->input->get("nom");
        $pu=$this->input->get("prix");
        $img=$this->input->get("image");
        $c->insertProduit($idCat,$nom,$pu,$img);
        $data=array();
        $data['dtP']=$c->getDetailsProduits();
       // $data['ii']=$idCat;
       $this->load->helper('css_helper');
       $data['page']='ListeProduits.php';
       $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }

    public function updateProduit(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $idCat=$this->input->get("categorie");
        $nom=$this->input->get("nom");
        $before=$this->input->get("before");
        $rep=$c->getDProduits($before);
        $pu=$this->input->get("prix");
        $img=$this->input->get("image");
        $idc=$rep[1]['idP'];
        $this->load->helper('css_helper');
        $c->updateProduit($idc,$idCat,$nom,$pu,$img);
        $data=array();
        $data['dtP']=$c->getDetailsProduits();
        $data['page']='ListeProduits.php';
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }

    public function selectDetailsProduitsProduit(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $rep=$c->getDetailsProduits();
        $this->load->helper('css_helper');
        $data=array();
        $data['dtP']=$rep;
        $data['page']='ListeProduits.php';
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
        /*sdsdsd */
    }

    public function deleteProduit(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $idpp=$this->input->post("idp");
        $this->load->helper('css_helper');
        $c->deleteProduit($idpp);
        $rep=$c->getDetailsProduits();
        $data=array();
        $data['dtP']=$rep;
        $data['page']='ListeProduits.php';
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }

    public function lienParam(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $data=array();
        $data['page']='Parametre.php';
        $data['Cat']=$c->getDetailsProduits();
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }

    public function insertParam(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $per=$this->input->get("date");
        $rem=$this->input->get("remise");
        $idp=$this->input->get("categorie");
        $c->insertParametrage($per,$rem,$idp);
        $rep=$c->getDetailsProduits();
        $data=array();
        $data['dtP']=$rep;
        $data['page']='ListeProduits.php';
        $data['nomCat']=$c->getCategorie();
        $this->load->view('templateBack',$data);
    }

	
	
}
