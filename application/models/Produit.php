<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    require('connection.php');
    class Produit extends CI_Model{
        private $id;
        private $nom;
        private $prix;
        private $stock;

        function listeProduit()
        {
                $j=1;
                $t=null;
                $sql="SELECT * FROM Produits";
               // $sql=sprintf($sql,$id);
                $news_req = mysqli_query(dbconnect(),$sql);
                while($don=mysqli_fetch_assoc($news_req))
                {
                $t[$j]['idP']=$don['idP'];
                $t[$j]['idCat']=$don['idCat'];
                $t[$j]['nom']=$don['nom'];
                $t[$j]['prixUnitaire']=$don['prixUnitaire'];
                $t[$j]['img']=$don['img'];
                $j++;
                }
                mysqli_free_result($news_req);    
                return $t;
        }

        /**
         * Get the value of id
         */
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of nom
         */
        public function getNom()
        {
                return $this->nom;
        }

        /**
         * Set the value of nom
         *
         * @return  self
         */
        public function setNom($nom)
        {
                $this->nom = $nom;

                return $this;
        }

        /**
         * Get the value of prix
         */
        public function getPrix()
        {
                return $this->prix;
        }

        /**
         * Set the value of prix
         *
         * @return  self
         */
        public function setPrix($prix)
        {
                $this->prix = $prix;

                return $this;
        }

        /**
         * Get the value of stock
         */
        public function getStock()
        {
                return $this->stock;
        }

        /**
         * Set the value of stock
         *
         * @return  self
         */
        public function setStock($stock)
        {
                $this->stock = $stock;

                return $this;
        }
    }

	
?>