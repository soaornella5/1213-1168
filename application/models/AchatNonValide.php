<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    // require('Mere.php');
    /*
    idANV int not null auto_increment,
    idP int,
    idC int,
    quantite int,
    prixTotal decimal,
    */
    class AchatNonValide extends CI_Model{
        private $idANV;
        private $idP;
        private $idC;
        private $quantite;
        private $prixTotal;

        /*public function insert(){
            $cur='curdate()';
            $req=('insert into achat (idCaisse,idProduit,quantite,dateAchat) VALUES ('.$this->getIdCaisse().','.$this->getIdProduit().','.$this->getQuantite().','.$cur.')');
            $this->db->query($req);
        }*/

        public function getIdANV() { 
            return $this->idANV; 
        } 

        public function setIdANV($id) {  
            $this->idANV = $id; 
        }

        public function getIdP() { 
            return $this->idP; 
        } 

        public function setIdP($id){  
            $this->idP = $id; 
        }

        public function getIdC() { 
            return $this->idC; 
        } 

        public function setIdC($id) {  
            $this->idC = $id; 
        }

        public function getQuantite() { 
            return $this->quantite; 
        } 

        public function setQuantite($id) {  
            $this->quantite = $id; 
        }

        public function getPrixTotal() { 
            return $this->prixTotal; 
        } 

        public function setPrixTotal($id) {  
            $this->prixTotal = $id; 
        }



       
}
?>