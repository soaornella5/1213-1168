<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class loginController extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

    public function selectUtilisateur(){
        $this->load->model('Fonction');
		$c=new Fonction();
        $n=$this->input->post("nom");
        $m=$this->input->post("mdp");
        $rep=$c->getUtilisateur($n,$m);
        if($rep!=null){
        $data=array();
        $data['dtP']=$c->getDetailsProduits();
        $data['utilisateur']=$rep;
        $data['page']='ListeProduits.php';
        $this->load->view('templateBack',$data);
        }
        else{
            $this->load->view('login.php');
        }
    }
	
}
