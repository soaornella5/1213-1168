create database caisse;
use caisse;
create table Parametrage(
    idParam int not null auto_increment,
    perenption date,
    remise float,
    idP int,
    primary key(idParam),
    foreign key (idP) references Produits(idP)
)ENGINE=InnoDB AUTO_INCREMENT=5;


create table Categorie(
    idCat int not null auto_increment,
    nom varchar(50),
    primary key(idCat)
)ENGINE=InnoDB AUTO_INCREMENT=5;
insert into Categorie values(1,'jeu de societe');
insert into Categorie values(2,'console');
insert into Categorie values(3,'jouets feminines');
create table Produits(
    idP int not null auto_increment,
    idCat int,
    nom varchar(50),
    prixUnitaire decimal,
    img varchar(50),
    primary key(idP),
    foreign key (idCat) references Categorie(idCat)
)ENGINE=InnoDB AUTO_INCREMENT=20;
insert into Produits values (1,1,'domino',10000,'domino.jpg');
insert into Produits values (2,1,'echec',11000,'echec.jpg');
insert into Produits values (3,1,'scarbble',12000,'scrabble.jpg');
insert into Produits values (4,1,'1000 bornes',12000,'bornes.jpg');
insert into Produits values (5,1,'monopoly',20000,'monopoly.jpg');
insert into Produits values (6,2,'ps1',100000,'ps1.jpg');
insert into Produits values (7,2,'ps2',150000,'ps2.jpg');
insert into Produits values (8,2,'ps3',750000,'ps3.jpg');
insert into Produits values (9,2,'ps4',1500000,'ps4.jpg');
insert into Produits values (10,2,'ps5',4000000,'xbox360.jpg');
insert into Produits values (11,3,'phone',20000,'phone.jpg');
insert into Produits values (12,3,'ordi',30000,'ordi.jpg');
insert into Produits values (13,3,'cuisine',18000,'cuisine.jpg');
insert into Produits values (14,3,'nounours',40000,'nounours.jpg');
insert into Produits values (15,3,'poupee reine des neiges',25000,'reinejpg.jpg');
create table Utilisateur(
    idU int not null auto_increment,
    nom varchar(50),
    mdp varchar(50),
    primary key(idU)
)ENGINE=InnoDB AUTO_INCREMENT=3;
insert into Utilisateur values (1,'Soa',sha1('1234'));
insert into Utilisateur values (2,'Nick',sha1('1234'));
create table Caisse(
    idC int,
    numero int,
    nom varchar(50),
    primary key(idC)
)ENGINE=InnoDB AUTO_INCREMENT=6;
insert into Caisse values (1,1,'Caisse 1');
insert into Caisse values (2,2,'Caisse 2');
insert into Caisse values (3,3,'Caisse 3');
create table AchatNonValide(
    idANV int not null auto_increment,
    idP int,
    idC int,
    quantite int,
    prixTotal decimal,
    primary key(idANV),
    foreign key (idP) references Produits(idP),
    foreign key (idC) references Caisse(idC)
)ENGINE=InnoDB AUTO_INCREMENT=3;
insert into AchatNonValide values(1,8,3,12,1500);
insert into AchatNonValide values(2,1,2,5,1900);
insert into AchatNonValide values(3,10,3,20,50000);
insert into AchatNonValide values(4,9,3,6,20000);
create table AchatValide(
    idAV int not null auto_increment,
    idANV int,
    dateAchat date,
    primary key(idAV),
    foreign key (idANV) references AchatNonValide(idANV)
)ENGINE=InnoDB AUTO_INCREMENT=3;
insert into AchatValide values(1,3,'2021-12-01');
insert into AchatValide values(2,1,'2021-12-25');

create view  DetailsProduit as
    select p.nom,p.prixUnitaire,p.img,p.idCat,c.nom as categorie,p.idP from Produits p
        join Categorie c on p.idCat=c.idCat;

create view DetailsAchat as
    select p.nom,p.img,av.quantite,av.prixTotal,c.numero as numeroCaisse,a.dateAchat from AchatValide a
        join AchatNonValide av on a.idANV=av.idANV
        join Produits p on av.idP=p.idP
        join Caisse c on av.idC=c.idC;

