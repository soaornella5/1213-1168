<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    // require('Mere.php');
    /*
    idAV int not null auto_increment,
    idANV int,
    dateAchat date,
    */
    class AchatValide extends CI_Model{
        private $idAV;
        private $idANV;
        private $dateAchat;

        /*public function insert(){
            $cur='curdate()';
            $req=('insert into achat (idCaisse,idProduit,quantite,dateAchat) VALUES ('.$this->getIdCaisse().','.$this->getIdProduit().','.$this->getQuantite().','.$cur.')');
            $this->db->query($req);
        }*/

        public function getIdAV() { 
            return $this->idAV; 
        } 

        public function setIdAV($id) {  
            $this->idAV = $id; 
        }

        public function getIdANV() { 
            return $this->idANV; 
        } 

        public function setIdANV($id) {  
            $this->idANV = $id; 
        }

        public function getDateAchat() { 
            return $this->dateAchat; 
        } 

        public function setDateAchat($id) {  
            $this->dateAchat = $id; 
        }

       
       
}
?>