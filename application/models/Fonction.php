<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    require('connection.php');
    /*
    idCat int not null auto_increment,
    nom varchar(50),
    */
class Fonction extends CI_Model{

    function listeProduit()
    {
            $j=1;
            $t=null;
            $sql="SELECT * FROM Produits";
        // $sql=sprintf($sql,$id);
            $news_req = mysqli_query(dbconnect(),$sql);
            while($don=mysqli_fetch_assoc($news_req))
            {
            $t[$j]['idP']=$don['idP'];
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['prixUnitaire']=$don['prixUnitaire'];
            $t[$j]['img']=$don['img'];
            $j++;
            }
            mysqli_free_result($news_req);    
            return $t;
    }
    function rechercheProduitSimple($mot)
    {
        $j=1;
        $t=null;
        $sql="SELECT * FROM DetailsProduit where nom like '%".$mot."%'";
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
            $t[$j]['idP']=$don['idP'];
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['prixUnitaire']=$don['prixUnitaire'];
            $t[$j]['img']=$don['img'];
            $t[$j]['categorie']=$don['categorie'];
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }

    function getDetailsProduits()
    {
        $j=1;
        $t=null;
        $sql="SELECT * FROM DetailsProduit";
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
            $t[$j]['idP']=$don['idP'];
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['prixUnitaire']=$don['prixUnitaire'];
            $t[$j]['img']=$don['img'];
            $t[$j]['categorie']=$don['categorie'];
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }

    function getDProduits($nom)
    {
        $j=1;
        $t=null;
        $sql="SELECT * FROM DetailsProduit where nom='%s'";
        $sql=sprintf($sql,$nom);
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
            $t[$j]['idP']=$don['idP'];
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['prixUnitaire']=$don['prixUnitaire'];
            $t[$j]['img']=$don['img'];
            $t[$j]['categorie']=$don['categorie'];
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }

    function getCategorie()
    {
        $j=1;
        $t=null;
        $sql="SELECT * FROM Categorie";
       
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
          
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }
    function top3Global()
    {
        $j=1;
        $t=null;
        $sql="select sum(quantite) as nb,sum(prixTotal) as prix,nom,img from DetailsAchat group by nom order by nb desc limit 3;";
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
            $t[$j]['img']=$don['img'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['nb']=$don['nb'];
            $t[$j]['prix']=$don['prix'];
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }
    function filtre($cat,$min,$max)
    {
        $j=1;
        $t=null;
        $sql="SELECT * FROM DetailsProduit where idCat='%s' and prixUnitaire>'%s' and prixUnitaire<'%s'";
        $sql=sprintf($sql,$cat,$min,$max);
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
            $t[$j]['idP']=$don['idP'];
            $t[$j]['idCat']=$don['idCat'];
            $t[$j]['nom']=$don['nom'];
            $t[$j]['prixUnitaire']=$don['prixUnitaire'];
            $t[$j]['img']=$don['img'];
            $t[$j]['categorie']=$cat;
            $j++;
        }
        mysqli_free_result($news_req); 
        return $t;
    }
    function listeProduitTop3(){
        $j=1;
        $t=null;
        $sql="SELECT * FROM Produits";
        // $sql=sprintf($sql,$id);
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
        $t[$j]['idP']=$don['idP'];
        $t[$j]['idCat']=$don['idCat'];
        $t[$j]['nom']=$don['nom'];
        $t[$j]['prixUnitaire']=$don['prixUnitaire'];
        $t[$j]['img']=$don['img'];
        $j++;
        }
        mysqli_free_result($news_req);    
        return $t;
    }
    function statProduit()
    {
            $j=1;
            $t=null;
            $sql="SELECT week(dateAchat)  as semaine, sum(quantite) as nb, sum(prixTotal) as prix FROM DetailsAchat where dateAchat<'2021-12-12' and dateAchat>'2021-01-01' group by semaine ";
            // $sql=sprintf($sql,$id);
            $news_req = mysqli_query(dbconnect(),$sql);
            while($don=mysqli_fetch_assoc($news_req))
            {
            $t[$j]['semaine']=$don['semaine'];
            $t[$j]['nb']=$don['nb'];
            $t[$j]['prix']=$don['prix'];
            $j++;
            }
            mysqli_free_result($news_req);    
            return $t;
    }
    function insertProduit($idCat,$nom,$pu,$img){
        $sql="INSERT INTO Produits VALUES (null,'%s','%s','%s','%s')";
        $sql=sprintf($sql,$idCat,$nom,$pu,$img);
        mysqli_query(dbconnect(), $sql);
    }
      
    function deleteProduit($nom){
        $sql="Delete from Produits where idP='%s'";
        $sql=sprintf($sql,$nom);
        mysqli_query(dbconnect(), $sql);
    }

    function updateProduit($idP,$idCat,$nom,$pu,$img){
        $sql="Update Produits set idCat='%s',nom='%s',prixUnitaire='%s',img='%s' where idP='%s'";
        $sql=sprintf($sql,$idCat,$nom,$pu,$img,$idP);
        // $sql=sprintf($sql,$idP,$idCat,$nom,$pu,$img);
        mysqli_query(dbconnect(), $sql);
    }

/*---------------LOGIN------------------------- */

    function getUtilisateur($nom,$mdp){
        $j=1;
        $t=null;
        $sql="SELECT * FROM Utilisateur where nom='%s' and mdp=sha1('%s')";
        $sql=sprintf($sql,$nom,$mdp);
        $news_req = mysqli_query(dbconnect(),$sql);
        while($don=mysqli_fetch_assoc($news_req))
        {
        $t[$j]['idU']=$don['idU'];
        $t[$j]['nom']=$don['nom'];
        $t[$j]['mdp']=$don['mdp'];
        $j++;
        }
        mysqli_free_result($news_req);    
        return $sql;
    }

    function insertParametrage($per,$remis,$idp){
        $sql="INSERT INTO Parametrage VALUES (null,'%s','%s','%s')";
        $sql=sprintf($sql,$per,$remis,$idp);
        mysqli_query(dbconnect(), $sql);
    }
    
}